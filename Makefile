DEVICE := icesky

MODEM_IMAGE := firmware-update/NON-HLOS.bin
SBL_ELF := firmware-update/sbl1.mbn
APPSBOOT_MBN := firmware-update/emmc_appsboot.mbn

VERSION := $(shell strings $(MODEM_IMAGE) | sed -n 's|QC_IMAGE_VERSION_STRING\=MPSS\.BO\.\(.*\)|\1|p')

HASH_SBL := $(shell openssl dgst -r -sha1 $(SBL_ELF) | cut -d ' ' -f 1)
HASH_APPSBOOT := $(shell openssl dgst -r -sha1 $(APPSBOOT_MBN) | cut -d ' ' -f 1)

TARGET := RADIO-$(DEVICE)-$(VERSION).zip

# Build
# ==========

.PHONY: build
build: assert inspect $(TARGET)
	@echo Size: $(shell stat -f %z $(TARGET))

$(TARGET): META-INF firmware-update
	zip -r9 $@ $^

# Clean
# ==========

.PHONY: clean
clean:
	rm -f *.zip

# Assert
# ==========
.PHONY: assert
assert: $(SBL_ELF) $(APPSBOOT_MBN)
ifneq ($(HASH_SBL), d18e72411ae33bdb46f99bc947e286bf50d6f069)
	$(error SHA-1 of sbl1.elf mismatch)
endif
ifneq ($(HASH_APPSBOOT), cd188d109153a2a7e74c122a96942460ea268e09)
	$(error SHA-1 of emmc_appsboot.mbn mismatch)
endif

# Inspect
# ==========

.PHONY: inspect
inspect:
	@echo Target: $(TARGET)
	@echo Version: $(VERSION)
